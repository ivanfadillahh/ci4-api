<?php 

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\ApiModel; // Model
use App\Filters\Cors; // Filter 

class Customer extends ResourceController
{
    use ResponseTrait;
    
    // konstruktor untuk call semua model atau function
    // public function __construct()
    // {
    //     parent::__construct();
    //     $modelApi = new ApiModel();
    // }

    // get all user
    public function index(){
        $apiModel = new ApiModel();
        $users = $apiModel->orderBy('id', 'ASC')->findAll();
        if($users){
            $code = 200;
            $data = [
                'status' => true,
                'message' => 'Berhasil menampilkan semua pengguna',
                'data' => $users
            ];
        }
        else{
            $code  = 200;
            $data = [
                'status' => false,
                'message' => 'Pengguna tidak ditemukan!',
                'data' => []
            ];
        }
        return $this->respond($data,$code);
    }

    // create user
    public function create() {
        $apiModel = new ApiModel();

        // value di headers
        // $this->request->getHeader(value: );

        // value di body
        // $this->request->getVar(value)

        if($this->request->getVar('nama') == '' || $this->request->getVar('nama') == null){
            $code = 200;
            $response = [
                'status'   => false,
                'messages' => 'Nama pengguna tidak boleh kosong!'
            ];
        }
        elseif($this->request->getVar('email') == '' || $this->request->getVar('email') == null){
            $code = 200;
            $response = [
                'status'   => false,
                'messages' => 'Email pengguna tidak boleh kosong!'
            ];
        }
        else{
            $data = [
                'role' => 2,
                'nama' => $this->request->getVar('nama'),
                'email'  => $this->request->getVar('email'),
                'password' => md5('user123')
            ];
            $create_user = $apiModel->insert($data);
            if($create_user){
                $code = 201;
                $response = [
                    'status'   => true,
                    'messages' => 'Berhasil membuat pengguna baru.'
                ];
            }
            else{
                $code = 200;
                $response = [
                    'status'   => false,
                    'messages' => 'Gagal membuat pengguna baru!'
                ];
            }
        }
        return $this->respond($response,$code);
    }

    // find user by id
    public function show($id = null){
        $apiModel = new ApiModel();
        if($id == '' || $id == null){
            $users = $apiModel->orderBy('id', 'DESC')->findAll();
            $code = 200;
            $response = [
                'status' => true,
                'message' => 'Berhasil menampilkan semua pengguna.',
                'data' => $users
            ];
        }
        else{
            $user = $apiModel->where('id', $id)->first();
            if($user){
                $code = 200;
                $response = [
                    'status' => true,
                    'message' => 'Berhasil menampilkan pengguna dengan id : '.$id,
                    'data' => $user
                ];
            }
            else{
                $code = 200;
                $response = [
                    'status' => false,
                    'message' => 'Pengguna dengan id : '.$id.' tidak ditemukan!',
                    'data' => []
                ];
            }
        }
        return $this->respond($response,$code);
    }

    // update user by id
    public function update($id = null){
        $apiModel = new ApiModel();
        $nama = $this->request->getVar('nama');
        if($id == '' || $id == null){
            $code = 200;
            $response = [
                'status' => false,
                'message' => 'Id pengguna harus di isi!',
                'data' => []
            ];
        }
        elseif($nama == '' || $nama == null){
            $code = 200;
            $response = [
                'status' => false,
                'message' => 'Nama pengguna harus di isi!',
                'data' => []
            ];
        }
        else{
            $data = [
                'nama' => $this->request->getVar('nama'),
                'email' => $this->request->getVar('email'),
            ];
            
            $update_user = $apiModel->update($id, $data);
            if($update_user){
                $code = 200;
                $response = [
                    'status'   => true,
                    'messages' => 'Berhasil update pengguna dengan Id : '.$id,
                ];
            }
            else{
                $code = 200;
                $response = [
                    'status'   => false,
                    'messages' => 'Gagal update pengguna dengan Id : '.$id.'!'
                ];
            }
        }
        return $this->respond($response,$code);
    }

    // delete user by id
    public function delete($id = null){
        $apiModel = new ApiModel();
        if($id == '' || $id == null){
            $code = 200;
            $response = [
                'status' => false,
                'message' => 'Id pengguna harus diisi!',
            ];
        }
        else{
            $data = $apiModel->where('id', $id)->delete($id);
            if($data){
                $delete_user = $apiModel->delete($id);
                if($delete_user){
                    $code = 200;
                    $response = [
                        'status' => true,
                        'message' => 'Berhasil menghapus pengguna dengan id : '.$id
                    ];
                }
                else{
                    $code = 200;
                    $response = [
                        'status' => false,
                        'message' => 'Gagal menghapus pengguna dengan id : '.$id.'!'
                    ];
                }
            }
        }
        return $this->respond($response,$code);
    }
}